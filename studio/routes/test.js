var express = require('express');
var router = express.Router();

/* GET test listing. */
router.get('/', function(req, res, next) {
    res.send('This is a test page');
});

module.exports = router;